       IDENTIFICATION DIVISION.
       PROGRAM-ID.  TR0004.
      *****************************************************************
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT 000-INPUT-FILE ASSIGN
               TO "C:/Users/ns0112/TRN-000x/TN000004/FILE-INPUT.dat"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-FILE-STATUS.

           SELECT 100-OUTPUT-FILE ASSIGN
               TO "C:/Users/ns0112/TRN-000x/TN000004/CR04.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-FILE-STATUS.

           SELECT 200-OUTPUT-FILE ASSIGN
               TO "C:/Users/ns0112/TRN-000x/TN000004/CR04J.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-FILE-STATUS.

           SELECT 300-OUTPUT-FILE ASSIGN
               TO "C:/Users/ns0112/TRN-000x/TN000004/CR04M.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-FILE-STATUS.

           SELECT 400-OUTPUT-FILE ASSIGN
               TO "C:/Users/ns0112/TRN-000x/TN000004/CR04V.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-FILE-STATUS.

      *****************************************************************
       DATA DIVISION.
       FILE SECTION.
       FD 000-INPUT-FILE.
       01 000-INPUT-RECORD.
           05 000-CARD-NUM                 PIC X(16).
           05 FILLER                       PIC X VALUE SPACE.
           05 000-AMOUNT                   PIC 9(9).
           05 FILLER                       PIC X VALUE SPACE.
           05 000-MDR                      PIC 9(8).
           05 FILLER                       PIC X VALUE SPACE.
           05 000-VAT                      PIC 9(7).
           05 FILLER                       PIC X VALUE SPACE.
           05 000-CARD-TYPE                PIC X(11).

       FD 100-OUTPUT-FILE.
       01 100-OUTPUT-RECORD.
           05 100-CARD-NUM                 PIC X(16).
           05 100-AMOUNT                   PIC 9(9).
           05 100-MDR                      PIC 9(8).
           05 100-VAT                      PIC 9(7).
           05 100-CARD-TYPE                PIC X(11).

       FD 200-OUTPUT-FILE.
       01 200-OUTPUT-RECORD.
           05 200-CARD-NUM                 PIC X(16).
           05 200-AMOUNT                   PIC 9(9).
           05 200-MDR                      PIC 9(8).
           05 200-VAT                      PIC 9(7).
           05 200-CARD-TYPE                PIC X(11).

       FD 300-OUTPUT-FILE.
       01 300-OUTPUT-RECORD.
           05 300-CARD-NUM                 PIC X(16).
           05 300-AMOUNT                   PIC 9(9).
           05 300-MDR                      PIC 9(8).
           05 300-VAT                      PIC 9(7).
           05 300-CARD-TYPE                PIC X(11).

       FD 400-OUTPUT-FILE.
       01 400-OUTPUT-RECORD.
           05 400-CARD-NUM                 PIC X(16).
           05 400-AMOUNT                   PIC 9(9).
           05 400-MDR                      PIC 9(8).
           05 400-VAT                      PIC 9(7).
           05 400-CARD-TYPE                PIC X(11).

      *****************************************************************
       WORKING-STORAGE SECTION.
       01  WS00293.
           05  WS-FILE-STATUS         PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.

       01 FORMATE-DATE-TIME.
           05 FM-DATE                      PIC X(12).
           05 FM-TIME                      PIC X(12).
       01 WS-CALCULATION.
           05 WS-RECORD-COUNT.
               10 WS-000-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-100-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-200-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-300-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-400-REC-COUNT         PIC S9(5) VALUE ZERO.
           05 WS-SUB                       PIC 9(2).
           05 WS-LINE-COUNT                PIC S9(3) VALUE +55.
           05 WS-LINE-MAX                  PIC S9(3) VALUE +55.
           05 WS-LINE-HEADER               PIC S9(3) VALUE +5.
       05 WS-SPACE-LINE                    PIC X 
                                               VALUE SPACE.
       01 WS-HEADER-REPORT-LINE-1-FILE-1.
           05 FILLER                       PIC X(48)
                                               VALUE "REPORT-NO: CRO4V".
           05 FILLER                       PIC X(18)
                                             VALUE "INCOME VISA REPORT".
           05 FILLER                       PIC X(51)
                                               VALUE SPACE.
           05 FILLER                       PIC X(7)
                                               VALUE "DATE : ".
           05 DAY1                         PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE "/".
           05 MONTH1                       PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE "/".
           05 YEAR1                        PIC X(2).
       01 WS-HEADER-REPORT-LINE-2-FILE-1.
           05 FILLER                       PIC X(48)
                                             VALUE "PROGRAM  : TRR0004".
           05 FILLER                       PIC X(69)
                                               VALUE SPACE.
           05 FILLER                       PIC X(7)
                                               VALUE "TIME : ".
           05 HOUR1-2                      PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE "/".
           05 MIN1-2                       PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE "/".
           05 SEC1-2                       PIC X(2).
       01 WS-HEADER-REPORT-LINE-1-FILE-2.
           05 FILLER                       PIC X(45)
                                               VALUE "REPORT-NO: CRO4M".
           05 FILLER                       PIC X(24)
                                       VALUE "INCOME MASTERCARD REPORT".
           05 FILLER                       PIC X(48)
                                               VALUE SPACE.
           05 FILLER                       PIC X(7)
                                               VALUE "DATE : ".
           05 DAY2                         PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE "/".
           05 MONTH2                       PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE "/".
           05 YEAR2                        PIC X(2).
       01 WS-HEADER-REPORT-LINE-2-FILE-2.
           05 FILLER                       PIC X(48)
                                             VALUE "PROGRAM  : TRR0004".
           05 FILLER                       PIC X(69)
                                               VALUE SPACE.
           05 FILLER                       PIC X(7)
                                               VALUE "TIME : ".
           05 HOUR2-2                      PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE ":".
           05 MIN2-2                       PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE ":".
           05 SEC2-2                       PIC X(2).
       01 WS-HEADER-REPORT-LINE-1-FILE-3.
           05 FILLER                       PIC X(48)
                                               VALUE "REPORT-NO: CRO4J".
           05 FILLER                       PIC X(18)
                                              VALUE "INCOME JCB REPORT".
           05 FILLER                       PIC X(51)
                                               VALUE SPACE.
           05 FILLER                       PIC X(7)
                                               VALUE "DATE : ".
           05 DAY3                         PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE "/".
           05 MONTH3                       PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE "/".
           05 YEAR3                        PIC X(2).
       01 WS-HEADER-REPORT-LINE-2-FILE-3.
           05 FILLER                       PIC X(48)
                                           VALUE "PROGRAM  : TRR0004".
           05 FILLER                       PIC X(69)
                                               VALUE SPACE.
           05 FILLER                       PIC X(7)
                                               VALUE "TIME : ".
           05 HOUR3-2                      PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE ":".
           05 MIN3-2                       PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE ":".
           05 SEC3-2                       PIC X(2).
       01 WS-HEADER-REPORT-LINE-1-FILE-4.
           05 FILLER                       PIC X(51)
                                               VALUE "REPORT-NO: CRO4".
           05 FILLER                       PIC X(18)
                                               VALUE "INCOME REPORT".
           05 FILLER                       PIC X(48)
                                               VALUE SPACE.
           05 FILLER                       PIC X(7)
                                               VALUE "DATE : ".
           05 DAY4                         PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE "/".
           05 MONTH4                       PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE "/".
           05 YEAR4                        PIC X(2).
       01 WS-HEADER-REPORT-LINE-2-FILE-4.
           05 FILLER                       PIC X(48)
                                           VALUE "PROGRAM  : TRR0004".
           05 FILLER                       PIC X(69) VALUE SPACE.
           05 FILLER                       PIC X(7)
                                               VALUE "TIME : ".
           05 HOUR4-2                      PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE ":".
           05 MIN4-2                       PIC X(2).
           05 FILLER                       PIC X(1)
                                               VALUE ":".
           05 SEC4-2                       PIC X(2).

       PROCEDURE DIVISION.
       0000-MAIN.
           PERFORM 1000-INITAIL    THRU 1000-EXIT.
           PERFORM 2000-PROCESS    THRU 2000-EXIT
                                   UNTIL FILE-AT-END.
           PERFORM 3000-END        THRU 3000-EXIT.
           STOP RUN.
       0000-EXIT.
           EXIT.
       1000-INITAIL.
           ACCEPT FM-DATE FROM DATE.
           ACCEPT FM-TIME FROM TIME.

           MOVE FM-DATE(5:2)       TO DAY1 DAY2 DAY3 DAY4.
           MOVE FM-DATE(3:2)       TO MONTH1 MONTH2 MONTH3 MONTH4.
           MOVE FM-DATE(1:2)       TO YEAR1 YEAR2 YEAR3 YEAR4.

           MOVE FM-TIME(1:2)       TO HOUR1-2 HOUR2-2 HOUR3-2 HOUR4-2.
           MOVE FM-TIME(3:2)       TO MIN1-2 MIN2-2 MIN3-2 MIN4-2.
           MOVE FM-TIME(5:2)       TO SEC1-2 SEC2-2 SEC3-2 SEC4-2.

           OPEN INPUT 000-INPUT-FILE
               OUTPUT 100-OUTPUT-FILE
                      200-OUTPUT-FILE
                      300-OUTPUT-FILE
                      400-OUTPUT-FILE.
           PERFORM 9100-CHK-FILE.
           PERFORM 8000-READ-FILE          THRU 8000-EXIT.

           GOBACK.
       1000-EXIT.
           EXIT.
       2000-PROCESS.
           PERFORM 5000-CHK-HEADER         THRU 5000-EXIT.
           PERFORM 6000-MOVE-OUTPUT        THRU 6000-EXIT.
           PERFORM 8000-READ-FILE          THRU 8000-EXIT.
           GOBACK.
       2000-EXIT.
           EXIT.
       3000-END.
           

           CLOSE 000-INPUT-FILE
                 100-OUTPUT-FILE
                 200-OUTPUT-FILE
                 300-OUTPUT-FILE
                 400-OUTPUT-FILE.
           PERFORM 9100-CHK-FILE.
           GOBACK.
       3000-EXIT.
           EXIT.



       5000-CHK-HEADER.
           ADD +1 TO WS-LINE-COUNT
           GOBACK.
       5000-EXIT.
           EXIT.
       6000-MOVE-OUTPUT.
           
           GOBACK.
       6000-EXIT.
           EXIT.
       7000-WRITE-FILE-1.
      *    WRITE 100-OUTPUT-RECORD
           PERFORM 9100-CHK-FILE.
       .
       7000-EXIT.
           EXIT.
       8000-READ-FILE.

           GOBACK.
       8000-EXIT.
           EXIT.
       9000-ABEND.

           GOBACK.
       9000-EXIT.
           EXIT.
       9100-CHK-FILE.
           IF FILE-OK
               CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE"
              PERFORM 9000-ABEND THRU 9000-EXIT
           END-IF
           GOBACK.